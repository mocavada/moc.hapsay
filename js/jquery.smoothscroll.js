
//JQUERY Smooth Scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 600);
        return false;
      }
    }
  });
});

//JQUERY Current

$(document).ready(function () {
	$(window).scroll(function () {
		var y = $(this).scrollTop();
		$('nav ul li a').each(function (event) {
			if (y >= $($(this).attr('href')).offset().top - 90) {
				$('nav ul li a').not(this).removeClass('current');
				$(this).addClass('current');
			}
		});
	});
});