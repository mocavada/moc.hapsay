/**
 * A wrapper object that allows cross-browser AJAX calls
 * @constructor
 * @returns {XMLHttpRequest|ActiveXObject|false} the available AJAX object or false 
 * if it is not suported
 */
function Ajax(){
    if( window.XMLHttpRequest ){
        // standards-compliant browser detected
        return new window.XMLHttpRequest();
    } else {
        try{
            // an old Internet Explorer browser detected
            return new ActiveXObject( 'MSXML2.XMLHTTP.3.0' );
        } catch( e ) {
            // browser doesn't support AJAX
            console.log( 'Your browser does not support AJAX (XMLHttpRequest)' );
            return false;
        }
    }
}

function App(){

    var submitButton    = null;
    var emailInput      = null;
    var xhr             = null;
    var spinner         = null;
    
    function init(){
        console.log( 'App.init()' );
        
        submitButton    = document.getElementById( 'email-submit' );
        emailInput      = document.getElementById( 'email' );
        spinner         = document.getElementById( 'spinner' );
        
        submitButton.addEventListener( 'click', formSubmitted );
    }
    
    function validateEmail( email ) {
        var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailPattern.test( email.toLowerCase() );
    }
    
    function removeMessage(){
        var responseDiv = document.getElementById( 'response' );
        if( responseDiv ){
            responseDiv.parentNode.removeChild( responseDiv );
        }
    }
    
    function showMessage( message, type ){
        
        removeMessage();
        
        var responseDiv = document.createElement( 'div' );
        responseDiv.id = 'response';
        responseDiv.className = type;
        responseDiv.innerHTML = '<p>' + message + '</p>';
            
        document.body.insertBefore( responseDiv, 
                document.getElementById( 'email-test' ) );
    }
    
    function formSubmitted( e ){
        e.preventDefault();
        
        var email = emailInput.value;
        
        if( !validateEmail( email ) ){
            showMessage( 'Please enter a valid email address.', 'error' );
        } else {
            xhr = new Ajax();
            
            xhr.addEventListener( 'readystatechange', stateChange );
            xhr.open( 'POST', 'gateway.php', true );
            
            var formData = new FormData();
            formData.append( 'client_email', email );
            
            xhr.send( formData );
            
            removeMessage();
            
            spinner.style.display = 'block';
            
            submitButton.disabled = true;
            emailInput.disabled = true;
        }
    }
    
    function stateChange( e ){
        if( xhr.readyState == xhr.DONE ){
            var response = JSON.parse( xhr.responseText );
            
            spinner.style.display = 'none';
            
            submitButton.disabled = false;
            emailInput.disabled = false;
            
            if( response.status == 'success' ){
                showMessage( response.data.email, 'success' );
            } else if( response.status == 'fail' ){
                showMessage( response.data.email, 'error' );
            } else {
                showMessage( response.message, 'error' );
            }
        }
    }
    
    window.addEventListener( 'load', init );
}

var app = new App();