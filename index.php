<?php

// load configuration settings
include( 'includes/config.inc.php' );

// load helpful functions into memory
include( 'includes/functions.inc.php' );


// determine which page is being viewed
$page = DEFAULT_PAGE;

if( isset( $_GET[ 'page' ] ) ){

	$page = $_GET[ 'page' ];
}

// check login status

if( !is_public_page( $page, $public_pages ) ){  
	
	check_login();
}

// connect to mysql server
$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME ) or die( mysqli_connect_error() );

// load uploader functions into memory

include( 'includes/upload.inc.php' );


// load email news functions into memory

//include( 'includes/email.inc.php' );
	
// form handlers
switch( $_SERVER[ 'REQUEST_METHOD' ] ){
	case 'POST':

		switch( $_POST[ 'action' ] ) {

			case 'login':

				$errors 
					= log_user_in( $db, $_POST[ 'email' ], $_POST[ 'password' ] );
			break;
				
		}
	break;
		
	case 'GET':

		switch( $_GET[ 'action' ] ) {
			case 'delete':
				delete_task( $db, $_GET[ 'delete_id' ] );
			break;
				
			default:
				// unsupported request method, this is an error condition
			break;
		}
	break;

	default:
		// unsupported request method, this is an error condition
	break;
}

// generate a template path

	$template = "includes/templates/{$page}.tpl.php";

//	$template = "includes/templates/edit.tpl.php";

if( file_exists( $template ) ) {
	// the template for the page existed, so lets render the page

	$head = "includes/templates/components/head.tpl.php";
	
//	$footer = "includes/templates/components/footer.tpl.php";
	
	include $head;
	
	include $template;
	
	
	
} else {
	// the template for the page does not exist, so show a 404 error
	redirect( '?page=404' );

}
			
			
			
			