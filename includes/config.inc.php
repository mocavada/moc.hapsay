<?php

	session_start();

    /*** PHP Error Settings*/

    // show all errors, but not the notices
    error_reporting( E_ALL & ~E_NOTICE );

    // show errors in the browser
    ini_set( 'display_errors', 1 );

    /*** Timezone Settings*/

    date_default_timezone_set( 'America/Toronto' );

    /*** SITE Settings*/

    define( 'SITE_TITLE',   'Marc Cavada - Web Developer' );
    define( 'DEFAULT_PAGE', 'index' );
    define( 'URL_REWRITE',  true );
    define( 'SITE_ROOT',    'http://moc.hapsay.com/' );

	/*** Database Credentials (Live)*/

	define( 'DB_HOST',      'localhost' );
    define( 'DB_USER',      'hapsayco_moc' );
    define( 'DB_PASSWORD',  'M0c78922' );
    define( 'DB_NAME',      'hapsayco_moc_test' );
	
	/*** Prod Settings*/

//    define( 'SITE_TITLE',   'Marc Cavada - Web Developer' );
//    define( 'DEFAULT_PAGE', 'index' );
//    define( 'URL_REWRITE',  true );
//    define( 'SITE_ROOT',    'http://localhost:33333/moc.hapsay/' );

	/*** Database Credentials (Prod)*/

//	define( 'DB_HOST',      'localhost' );
//    define( 'DB_USER',      'root' );
//    define( 'DB_PASSWORD',  'M0c78922' );
//    define( 'DB_NAME',      'hapsayco_moc_test' );
	


    /*** File Upload Settings*/
	
	define( 'UPLOADS_FOLDER', 'uploads/' );
    define( 'ART_FOLDER', UPLOADS_FOLDER . 'art/' );
    define( 'SMALL_FOLDER', UPLOADS_FOLDER . 'small/' );
	define( 'LARGE_FOLDER', UPLOADS_FOLDER . 'large/' );


	//	Upload Check Settings
    define( 'ALLOWED_FILE_TYPES', 'image/png,image/gif,image/jpeg,image/pjpeg' );
	define( 'RANDOMIZE_FILENAMES' , 'true' );

    /*** File Extensions*/

		$mime_to_extension = array (
		'image/jpeg' => '.jpg',
		'image/pjpeg' => '.jpg',
		'image/gif' => '.gif',
		'image/png' => '.png'
	);

  
    /*** Login Settings*/

    // only value that indicates a logged in user
    define( 'LOGIN_TOKEN', 'N1cOsWvvezifyTfLuPE6ABCdPdJFp8RvLV1Qg4qr' );

    // pages that don't require a login
    $public_pages = array( '404', 'login', 'index' , 'preview', 'header' , 'header-page' ,'footer' );

