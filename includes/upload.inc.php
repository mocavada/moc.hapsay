 <?php

/**
     * Upload DATA from FORM Submit and INSERT TO DB
     *@return error ONLY for FILENAME
     * IMPROVE/ADD Error Message
*/

$errors = array();

//	INFO FUNCTION

if( isset( $_POST[ 'upload-started' ] ) ){
	// form has been submitted

	// is there a PROJECT Title?
	if( strlen( $_POST[ 'title' ] ) < 1 ){
		$errors[ 'title' ] = '<p class="error">Please enter a title for the image.</p>';
		

	 } else {

		//	FILE-UPLOAD FUNCTION	

		if( $_FILES[ 'file-upload' ][ 'error' ] == UPLOAD_ERR_OK ){

			// the file uploaded successfully to the temp folder

			// current location of the file (temporary folder and filename)
			$temp_location = $_FILES[ 'file-upload' ][ 'tmp_name' ];

			// retrieve image-specific information about the uploaded file
			// Note: if you want to upload non-images, this needs to be
			// changed to mime_content_type()
			$info = getimagesize( $temp_location );

			if( strpos( ALLOWED_FILE_TYPES, $info[ 'mime' ] ) !== false ){

			// the file's MIME type matched one of the types in our
			// ALLOWED_FILE_TYPES constant

				if( RANDOMIZE_FILENAMES ){
					// generate a filename, based on time and original filename
					// use a hash to get a filename-safe string
					$filename = sha1( microtime() . $_FILES[ 'file-upload' ][ 'name' ] );

					// get the correct extension for the file, from MIME type
					$extension = $mime_to_extension[ $info[ 'mime' ] ];

					$destination = UPLOADS_FOLDER . $filename . $extension;

				} else {
					// new folder combined with original filename
					$destination = UPLOADS_FOLDER . $_FILES[ 'file-upload' ][ 'name' ];
				}

				// attempt to move the file from the temp folder to the
				// new file path that was generated above
				if( move_uploaded_file( $temp_location, $destination ) ){
	
					$small  = resize_to_fit( $destination, 100, SMALL_FOLDER );
					
					$large  = resize_to_fit( $destination, 600, LARGE_FOLDER );
					

					// file was moved successfully
					$gallery = 
						"<h5>Your image was Successfully Uploaded.</h5>
						 <a href=\"$destination\">
							<img src=\"$small\" alt=\"medium preview\" />
						 </a>";
					
					$preview = 
						"<h5>Your image was Successfully Uploaded.</h5>
						 <a href=\"$destination\">
							<img src=\"$large\" alt=\"medium preview\" />
						 </a>";
					
					// insert array values in typ variable
					
						$types	= $_POST[ 'type' ];
					    $typ = "";
					
						foreach($types as $type) { $typ .= $type." "; }

					// gather the needed info
					
						$login			= $_SESSION[ 'login_id' ];
//						$login			= 1;
												
						$title			= $_POST[ 'title' ];
						$description	= $_POST[ 'description' ];
						$tech			= $_POST[ 'tech' ];
						$tags			= $_POST[ 'tags' ];
						$siteurl		= $_POST[ 'siteurl' ];
						$giturl			= $_POST[ 'giturl' ];
						
						$filename		= str_replace( UPLOADS_FOLDER , '', $destination );
						
						// sanitize the info
						
						$typ		    = sanitize( $db, $typ );
						$title    		= sanitize( $db, $title );
						$description    = sanitize( $db, $description );
						$tech    		= sanitize( $db, $tech );
						$siteurl    	= sanitize( $db, $siteurl );
						$giturl    		= sanitize( $db, $giturl );	

						$filename   	= sanitize( $db, $filename );
					

					$query = "INSERT INTO projects( login_id, type, title, description, tech, siteurl, giturl, file_name )

					VALUES ( '$login', '$typ', '$title', '$description', '$tech', '$siteurl', '$giturl', '$filename' )";

					$result = mysqli_query( $db, $query ) 
					or die( mysqli_error( $db ) .'<br>'. $query ) ;

//					// reset input values
					 $_POST[ 'typ' ]       		= '';
					
					 $_POST[ 'title' ]       	= '';	
					 $_POST[ 'description' ]	= '';	
					 $_POST[ 'tech' ]       	= '';
					 $_POST[ 'siteurl' ]       	= '';
					 $_POST[ 'giturl' ]       	= '';	
					

				} else {
					// likely causes:
					// - missing uploads folder
					// - permissions of folder are wrong
					// - destination filename is garbled up somehow
					$errors[ 'upload' ] = '<p class="error">
						There was a problem saving the file;
						please contact the administrator.
					</p>';
				}

			} else {

				$errors[ 'upload' ] = '<p class="error">
						The file uploaded is not one of the allowed
						file types: gif, jpeg or png.
					</p>';
			}

		} else {

			// there was some kind of server error

			switch( $_FILES[ 'file-upload' ][ 'error' ] ){

				case UPLOAD_ERR_INI_SIZE:
					$errors[ 'upload' ] = '<p class="error">
						The uploaded file exceeds the
						maximum allowed file size of ' 
						. ini_get( 'upload_max_filesize' ) 
						. ' </p>';
				break;
				case UPLOAD_ERR_NO_FILE:
					$errors[ 'upload' ] = '<p class="error">
						Please select a file to upload.</p>';
				break;
				case UPLOAD_ERR_CANT_WRITE:
					$errors[ 'upload' ] = '<p class="error">
						The server was unable to save the uploaded file.</p>';
				break;
				case UPLOAD_ERR_PARTIAL:
					$errors[ 'upload' ] = '<p class="error">
						The file was only partially uploaded.</p>';
				break;
				default:
					$errors[ 'upload' ] = '<p class="error">
						There was a server issue in uploading the file.</p>';
				break;
			}
		}

	}
	
	
}

/**
     * Upload DATA from FORM Submit and UPDATE THE ROW OF THE ID * * MARCHING THE SUBMITTED FORM TO DB
     *@return error ONLY for FILENAME
     * IMPOROVE/ADD Error Message
*/

    if( isset( $_POST[ 'edit-upload' ] ) ){
        // form has been submitted
        
        // is there a PROJECT Title?
        if( strlen( $_POST[ 'title' ] ) < 1 ){
            $errors[ 'title' ] = '<p class="error">Please enter a title for the image.</p>';
		
		 } else {
      
			//	FILE-UPLOAD FUNCTION	

			if( $_FILES[ 'edit-upload' ][ 'error' ] == UPLOAD_ERR_OK ){

				// the file uploaded successfully to the temp folder

				// current location of the file (temporary folder and filename)
				$temp_location = $_FILES[ 'edit-upload' ][ 'tmp_name' ];

				// retrieve image-specific information about the uploaded file
				// Note: if you want to upload non-images, this needs to be
				// changed to mime_content_type()
				$info = getimagesize( $temp_location );

				if( strpos( ALLOWED_FILE_TYPES, $info[ 'mime' ] ) !== false ){

				// the file's MIME type matched one of the types in our
				// ALLOWED_FILE_TYPES constant

					if( RANDOMIZE_FILENAMES ){
						// generate a filename, based on time and original filename
						// use a hash to get a filename-safe string
						$filename = sha1( microtime() . $_FILES[ 'edit-upload' ][ 'name' ] );

						// get the correct extension for the file, from MIME type
						$extension = $mime_to_extension[ $info[ 'mime' ] ];

						$destination = UPLOADS_FOLDER . $filename . $extension;

					} else {
						// new folder combined with original filename
						$destination = UPLOADS_FOLDER . $_FILES[ 'edit-upload' ][ 'name' ];
					}

					// attempt to move the file from the temp folder to the
					// new file path that was generated above
					if( move_uploaded_file( $temp_location, $destination ) ){
						
					$small  = resize_to_fit( $destination, 100, SMALL_FOLDER );
					
					$large  = resize_to_fit( $destination, 600, LARGE_FOLDER );
					

						// file was moved successfully
						$gallery = 
							"<h5>Your image was Successfully Uploaded.</h5>
							 <a href=\"$destination\">
								<img src=\"$small\" alt=\"medium preview\" />
							 </a>";
					
						$preview = 
							"<h5>Your image was Successfully Uploaded.</h5>
							 <a href=\"$destination\">
								<img src=\"$large\" alt=\"medium preview\" />
							 </a>";
                        
	                   // insert array values in typ variable
					
						$types	= $_POST[ 'type' ];
					    $typ = "";
					
						foreach($types as $type) { $typ .= $type." "; }

					// gather the needed info
					
						$login			= $_SESSION[ 'login_id' ];

												
						$title			= $_POST[ 'title' ];
						$description	= $_POST[ 'description' ];
						$tech			= $_POST[ 'tech' ];
						$tags			= $_POST[ 'tags' ];
						$siteurl		= $_POST[ 'siteurl' ];
						$giturl			= $_POST[ 'giturl' ];
						
						$filename		= str_replace( UPLOADS_FOLDER , '', $destination );
						
						// sanitize the info
						
						$typ		   = sanitize( $db, $typ );
						$title    		= sanitize( $db, $title );
						$description    = sanitize( $db, $description );
						$tech    		= sanitize( $db, $tech );
						$siteurl    	= sanitize( $db, $siteurl );
						$giturl    		= sanitize( $db, $giturl );	

						$filename   	= sanitize( $db, $filename );
					
						
$query = "UPDATE projects SET

type = '$typ', title = '$title', description = '$description', tech = '$tech', siteurl = '$siteurl', giturl = '$giturl', file_name = '$filename' 

WHERE id = {$_GET[ 'edit_id' ]}";
								

						$result = mysqli_query( $db, $query ) 
						or die( mysqli_error( $db ) .'<br>'. $query ) ;
						
						
							// reset input values
						
//						 $_POST[ 'title' ]       	= '';	
//						 $_POST[ 'description' ]   	= '';	
//						 $_POST[ 'tech' ]       	= '';
//						 $_POST[ 'tags' ]       	= '';
//						 $_POST[ 'docurl' ]       	= '';
//						 $_POST[ 'siteurl' ]       	= '';	
//						 $_POST[ 'art' ]       		= '';	
//						 $_POST[ 'draft' ]       	= '';	
//						 $_POST[ 'display' ]       	= '';	
							   

					} else {
						// likely causes:
						// - missing uploads folder
						// - permissions of folder are wrong
						// - destination filename is garbled up somehow
						$errors[ 'upload' ] = '<p class="error">
							There was a problem saving the file;
							please contact the administrator.
						</p>';
					}

				} else {

					$errors[ 'upload' ] = '<p class="error">
							The file uploaded is not one of the allowed
							file types: gif, jpeg or png.
						</p>';
				}

			} else {

				// there was some kind of server error

				switch( $_FILES[ 'edit-upload' ][ 'error' ] ){

					case UPLOAD_ERR_INI_SIZE:
						$errors[ 'upload' ] = '<p class="error">
							The uploaded file exceeds the
							maximum allowed file size of ' 
							. ini_get( 'upload_max_filesize' ) 
							. ' </p>';
					break;
					case UPLOAD_ERR_NO_FILE:
						$errors[ 'upload' ] = '<p class="error">
							Please select a file to upload.</p>';
					break;
					case UPLOAD_ERR_CANT_WRITE:
						$errors[ 'upload' ] = '<p class="error">
							The server was unable to save the uploaded file.</p>';
					break;
					case UPLOAD_ERR_PARTIAL:
						$errors[ 'upload' ] = '<p class="error">
							The file was only partially uploaded.</p>';
					break;
					default:
						$errors[ 'upload' ] = '<p class="error">
							There was a server issue in uploading the file.</p>';
					break;
				}
			}
			
		}
		
	
   }