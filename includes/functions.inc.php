<?php
    /**
     * Sanitizes a string to make it safe for database entry.
     * 
     * @param resource $db The database connection resource.
     * @param string $string The string to process.
     * @param boolean $strip_tags A flag to disable removal of HTML tags.
     *
     * @return string
     */
    function sanitize( $db, $string, $strip_tags = true ){
        
        $string = trim( $string );
            
        if( $strip_tags ){
            $string = strip_tags( $string );
        }

        $string = mysqli_real_escape_string( $db, $string );
        
        return $string;
    }

    /**
     * Redirects the browser to a new URL, or refreshes the
     * current page if the URL is not provided.
     * 
     * @param string $url The URL to redirect to.
     */
    function redirect( $url = false ){
        
        if( !$url ){
            
            $url = SITE_ROOT;
            
        } else {
            
            if( URL_REWRITE ){
                $url = str_replace( '?page=', '', $url );
            }
            
            $url = SITE_ROOT . $url;
        }
        
        header( 'Location: ' . $url );
        echo "Redirect failed, <a href=\"$url\">manually navigate</a>.";
        die();
    }

    /**
     * Checks if the page is private and if the user is logged in,
     * if not they are sent to the login page.
     */
    function check_login(){
        if( !is_logged_in() ){
            redirect( '?page=login' );
        }
    }


    /**
     * Returns the users log in status.
     *
     * @returns boolean
     */
    function is_logged_in(){
        return strcmp( LOGIN_TOKEN, $_SESSION[ 'logged_in' ] ) == 0;
    }   

    /**
     * Check if the current page is public or private.
     *
     * @param string $page The page name to check.
     * @param array $public_pages The pre-set array of public pages.
     *
     * @returns boolean
     */
    function is_public_page( $page, $public_pages ){
		
        return in_array( $page, $public_pages );
    }

    /**
     * Attempts to log a user into the app, 
     * returns error messages on failure.
     * 
     * @param resource $db The database connection resource.
     * @param string $email The user's email address.
     * @param string $password The user's password.
     *
     * @return array
     */
    function log_user_in( $db, $email, $password ){
		
        $errors = array();
        
        $email = sanitize( $db, $email );
        
        // is there an email
        if( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ){
            $errors[ 'email' ] 
                = '<p class="error">Please enter a valid email address.</p>';
        }
        
        // is there a password
        if( strlen( $password ) < 1 ){
            $errors[ 'password' ] 
                = '<p class="error">Please enter your password.</p>';
        }
        
        if( count( $errors ) == 0 ){
            // retrieve user record matching entered email
            $query = "SELECT login_id, password FROM login
                        WHERE email='$email' LIMIT 1";
			
            $result = mysqli_query( $db, $query ) 
                or die( mysqli_error( $db ) );
            
            if( mysqli_num_rows( $result ) == 1 ){
                // 1 record returned - user exists in our system
                
                $row = mysqli_fetch_assoc( $result );
                
                // compare entered password to stored password
                if( $password == $row[ 'password' ] ){
                    // match - log user in
                    
                    $_SESSION[ 'logged_in' ]   = LOGIN_TOKEN;
                    $_SESSION[ 'email' ]       = $email;
                    $_SESSION[ 'login_id' ]    = $row[ 'login_id' ];
  
					 redirect('?page=admin');
                    
                } else {
                    // no match - reject login
                    $errors[ 'password' ] 
                        = '<p class="error">The password was incorrect.</p>';
                }
            } else {
                // 0 records returned - no such user, or incorrect email
                $errors[ 'email' ] 
                    = '<p class="error">No such email, please check your login.</p>';
            }
        }
        
        return $errors;
    }

/**
 * Retrieves all the tasks from the tasks table.
 * @param resource $db The database connection resource.
 * @return resource
 */
	function query_all( $db ){
        // retrieve all existing tasks
        $query = "SELECT * FROM projects";
        
        $result = mysqli_query( $db, $query ) 
            or die( mysqli_error( $db ) .'<br>'. $query );
		
        return $result;
    }

/**
* Retrieves all the tasks from the tasks table.
* @param resource $db The database connection resource.
* @return resource
*/
    function query_projects( $db ){
        // retrieve all existing tasks
        
          $query = "SELECT * FROM projects
                    WHERE login_id = 1 OR 2 OR 3
                    ORDER BY date_created DESC";
        
        
//        $query = "SELECT * FROM projects
//                    WHERE login_id = {$_SESSION['login_id']}
//                    ORDER BY date_created DESC";
        
        $result = mysqli_query( $db, $query ) 
            or die( mysqli_error( $db ) .'<br>'. $query );
		
        return $result;
    }

/**
 * Retrieves all the tasks from the tasks table.
 * 
 * @param resource $db The database connection resource.
 *
 * @return resource
 */

	function query_edit_project( $db ){
        // retrieve all existing tasks
		
        $query = "SELECT * FROM projects
                    WHERE id = {$_GET['edit_id']} AND login_id = {$_SESSION['login_id']} LIMIT 1";
        
        $result = mysqli_query( $db, $query ) 
            or die( mysqli_error( $db ) .'<br>'. $query );
		
        
        return $result;
		
    }

   
    /**
     * Deletes the task that has the id of delete_id.
     * 
     * @param resource $db The database connection resource.
     * @param int $delete_id The id of the existing task to delete.
     */
    function delete_task( $db, $delete_id ){
		
        $delete_id = sanitize( $db, $delete_id );
        if( is_numeric( $delete_id ) ){
            
            $query = "DELETE FROM projects
                        WHERE id = $delete_id";
            
            $result = mysqli_query( $db, $query ) 
                or die( mysqli_error( $db ) );
		
			
            // drop all form data
            redirect( '?page=admin#projectList' );
        }
    }


    /**
     * Retrieves all the tasks from the tasks table.
     * 
     * @param resource $db The database connection resource.
     *
     * @return resource
     */

function resize_to_fit( $original_filename, 
                            $size, 
                            $destination_folder, 
                            $quality = 10 ){
        
        // determine original width, original height, mime type
        $info = getimagesize( $original_filename );
        
        // store the dimensions of the original image
        $original_width     = $info[ 0 ];
        $original_height    = $info[ 1 ];

        $isGIF = false;
        // read the original file into the web server's memory (RAM)
        switch( $info[ 'mime' ] ){
            case 'image/gif':
                $original = imagecreatefromgif( $original_filename );
                $isGIF = true;
            break;
            case 'image/png':
                $original = imagecreatefrompng( $original_filename );
            break;
            case 'image/jpeg':
            case 'image/pjpeg':
                $original = imagecreatefromjpeg( $original_filename );
            break;
            default:
                return false;
            break;
        }
        
        // calculate the aspect ratio (portrait/landscape/square)
        $aspect_ratio = $original_width / $original_height;
        
        // calculate the dimensions of the new image
        if( $aspect_ratio > 1 ){
            // landscape image
            $new_width  = ceil( $size );
            $new_height = ceil( $size / $aspect_ratio );
        } else {
            // portrait or square
            $new_height = ceil( $size );
            $new_width  = ceil( $size * $aspect_ratio );
        }
        
        /* echo "Width: $original_width <br />
              Height: $original_height <br />
              Aspect Ratio: $aspect_ratio <br />
              New Width: $new_width <br />
              New Height: $new_height <br />"; */
        
        // create an empty image using the new dimensions in memory
        $new_image = imagecreatetruecolor( $new_width, $new_height );
        
        if( !$isGIF ){
            // stop PHP from blending alpha channel with color channels
            // which would result in only opaque pixels (the default)
            imagealphablending( $original, false );
            imagealphablending( $new_image, false );

            // enable the alpha channel so partial transparency
            // can be stored in these images
            imagesavealpha( $original, true );
            imagesavealpha( $new_image, true );
        }
        
        // create a "transparent" colour to fill the new image with
        $transparent = imagecolorallocatealpha( $new_image, 0, 0, 0, 127 );
        
        // flood-fill (like paint bucket) the new image with transparent
        imagefill( $new_image, 0, 0, $transparent );
        
        // copy and resample pixels from original to new image in memory
        if( !imagecopyresampled( $new_image, 
                                 $original,
                                 0, 0,
                                 0, 0,
                                 $new_width,
                                 $new_height,
                                 $original_width,
                                 $original_height ) ){
            imagedestroy( $original );
            imagedestroy( $new_image );
            return false;
        }
        // write the new image to the appropriate folder
        
        // break the path apart into pieces, using '/'
        $file_parts = explode( '/', $original_filename );
        // keep the last item which is the filename
        $full_filename = array_pop( $file_parts );
        // break apart the filename using '.'
        $file_parts = explode( '.', $full_filename );
        array_pop( $file_parts );
        $final_filename = implode( '.', $file_parts );

        $final_file_path = $destination_folder . $final_filename;
        
        switch( $info[ 'mime' ] ){
            case 'image/gif':
                $final_file_path .= '.gif';
                $result = imagegif( $new_image, 
                                    $final_file_path );
            break;
            case 'image/png':
                // convert 0-10 to be 9-0 for the PNG function
                $png_quality = 9 - ( ( $quality / 10 ) * 9 );
                $final_file_path .= '.png';
                $result = imagepng( $new_image,
                                    $final_file_path,
                                    $png_quality );
            break;
            case 'image/jpeg':
            case 'image/pjpeg':
                $final_file_path .= '.jpg';
                $result = imagejpeg( $new_image, 
                                     $final_file_path,
                                     $quality * 10 );
            break;
            default:
                $result = false;
            break;
        }
        
        // release the memory used by the images
        imagedestroy( $original );
        imagedestroy( $new_image );
        
        if( !$result ){
            return false;
        }
        
        return $final_file_path;
    }
