<?php
    
    $_SESSION[ 'logged_in' ] = null;
    $_SESSION[ 'email' ]     = null;
    $_SESSION[ 'login_id' ]  = null;
   


    unset( $_SESSION[ 'logged_in' ] );
    unset( $_SESSION[ 'email' ] );
    unset( $_SESSION[ 'login_id' ] );
    
	header("Refresh:0");

    redirect();