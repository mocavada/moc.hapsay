<?php header( 'HTTP/1.0 404 Not Found' );  ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>404 Error Not Found - <?php echo SITE_TITLE; ?></title>
        
        <!-- link to the main stylesheet -->
        <link rel="stylesheet" href="css/style.css" />
        
        <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <h1><?php echo SITE_TITLE; ?></h1>
        </header>
        <main>
            <h2>404 Error Page Not Found</h2>
            <p>The page you are looking for does not exist.</p>
        </main>
    </body>
</html>