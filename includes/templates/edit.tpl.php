<?php

if( is_logged_in() ){
	
	$result = query_edit_project( $db );
	$row = mysqli_fetch_assoc( $result );
}

if( $_GET[ 'page' ]  == 'edit' ){

$result = query_projects( $db );
	
}

?>
<body>
	
<main id="adminContainer">
	
<section class="center tcenter fl">	
    
	    <h3 class="tcenter" >Modify Portfolio</h3>
	    
	    <img src="<?php echo SITE_ROOT . LARGE_FOLDER . $row[ 'file_name' ]; ?>" alt="Feature Image" >
		<h4 class="tcenter"><?php echo $row[ 'title' ]; ?></h4>
    
		
		<form id="admin" 
		action="<?php echo $_SERVER['REQUEST_URI' ]?>"
		method="post" enctype="multipart/form-data" >	

		<!--VERY IMPORTANT HIDDEN INPUT-->
			<input type="hidden" name="edit-upload" value="true" />			

			<!--COMMENT-->
		<h4>CODE LEVEL</h4>
		
		<label class="check">		
				<p>PHP</p>
				<input  type="checkbox" 
						name="type[]" 
						value="php"
						checked
						 />
				<span class="checkmark"></span>			
		</label>
		<label class="check">		
				<p>JAVASCRIPT</p>
				<input  type="checkbox" 
						name="type[]" 
						value="js" 
						/>
				<span class="checkmark"></span>			
		</label>
		<label class="check">		
				<p>FRAMEWORK</p>
				<input  type="checkbox" 
						name="type[]" 
						value="framework" 
						/>
				<span class="checkmark"></span>			
		</label>	


		<h4>PROJECT TITLE</h4>

		<?php echo $errors[ 'title' ]; ?>
		<input type="text" 
               name="title"
               placeholder="<?php echo $row[ 'title' ]; ?>"
               value="<?php echo $_POST[ 'title' ]; ?>" />

		<h4>SHORT DESCRIPTION</h4>	

		<div class="col33 wrapper">

		<?php echo $errors[ 'description' ]; ?>
			<div class="main">
				<?php echo $errors[ 'description' ]; ?>
			 <textarea name="description"
                       placeholder="<?php echo $row[ 'description' ]; ?>"
					   rows="3" cols="130"><?php echo $_POST[ 'description' ]; ?></textarea>
			</div>
		</div>
		
		<h4>TECHNOLOGY USED</h4>
		<?php echo $errors[ 'tech' ]; ?>
			<input 	type="text" 
					name="tech"
                    placeholder="<?php echo $row[ 'tech' ]; ?>"
					value="<?php echo $_POST[ 'tech' ]; ?>" />	
						
	
		<h4>PROJECT LINK</h4>

		<?php echo $errors[ 'siteurl' ]; ?>
		<input  type="text" 
				name="siteurl"
                placeholder="<?php echo $row[ 'siteurl' ]; ?>"
				value="<?php echo $_POST[ 'siteurl' ]; ?>" />	

		<h4>GIT REPOSITORY LINK</h4>

		<?php echo $errors[ 'docurl' ]; ?>
		<input 	type="text" 
				name="giturl"
                placeholder="<?php echo $row[ 'giturl' ]; ?>"
				value="<?php echo $_POST[ 'docurl' ]; ?>" />

<!--		UPLOAD-->
            
        <div class="col12 fl">
            <h4>FEATURE IMAGE</h4>
            <input type="file" class="file-upload" name="edit-upload" />
        </div>	


        <!--		SUBMIT-->
        <div class="col12 fr">
            <h4>UPDATE PORTFOLIO</h4>
            <br/>
            <div class="publish">
				<input type="submit" value="PUBLISH"/>
			</div>
        </div>
			
				
		</form>	
    
        <div class="clear"></div>
    
    </section>	
    
    <aside class="outline fr">	
        
		<span class="logout">

		<a class="tcenter" href="<?php echo ( URL_REWRITE ) ? 
						SITE_ROOT . 'logout' 
					  : SITE_ROOT . '?page=logout'; ?>">LOG OUT</a>
					  
		<a class="tcenter" href="<?php SITE_ROOT ?>admin">ADD PORTFOLIO</a>	

		</span>
        
		<div class="clear"></div>

<div id="projectList">    

<h3>Portfolio List</h3>

<table>

    <tr>
    <th>Date</th>
    <th>Title</th>
    <th>Type</th>
    <th>Image</th>
    <th colspan="2">Modify</th>
</tr>

    <?php while( $row = mysqli_fetch_assoc( $result ) ): ?>
<tr>
    <td><?php echo $row[ 'date_created' ]; ?></td>

    <td><?php echo $row[ 'title' ]; ?></td>

    <td><?php echo $row[ 'type' ]; ?></td>

    <td>
        <a href="?page=preview&amp;id=<?php echo $row[ 'id' ] ;?>">

        <img class="img-ic" src="<?php echo SMALL_FOLDER . $row[ 'file_name' ]; ?>" alt="Project Image" /></a>

    </td>

   <td><a href="../?page=edit&amp;action=edit&amp;edit_id=<?php echo $row['id']; ?>">
				<i class="fas fa-pencil-alt"></i>
	</a></td>

    <td>
        <a href="?action=delete&amp;delete_id=<?php echo $row['id']; ?>">
        <i class="fas fa-trash"></i>
        </a>
    </td>

</tr>

<?php endwhile; ?>

</table>

</div>

</aside>
			
<div class="clear"></div>	
	
</main>
</body>

</html>
