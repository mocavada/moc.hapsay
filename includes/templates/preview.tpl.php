
<?php

	$id = mysqli_real_escape_string
	( $db, strip_tags( trim( $_GET[ 'id' ] ) ) );

    if( !is_numeric( $id ) ){
    header('Location: index.php');
    exit();
	}

	$query = "SELECT * FROM projects WHERE id = $id";

	$result = mysqli_query( $db, $query ) or die(mysqli_error($db) . '<br />' . $query );
	
	$row = mysqli_fetch_assoc( $result );

?>

<body>
    
    <main class="main-container">
    
    <?php include( 'includes/templates/header-page.tpl.php' ); ?>
    
	
        <section id="previewContainer">	

            <div id="gallery" class="col13 fl">
               
                <img class="img-pv center" src="<?php echo SITE_ROOT . '/' . LARGE_FOLDER . $row[ 'file_name' ]; ?>" alt="Art Project" />
               
            </div>
            
            <div class="col13 fl">
            
            	<h3><?php echo ( $row['title'] ); ?></h3>

                <li><h4>About The Project</h4><p><?php echo $row[ 'description' ]; ?><p></li>	
                
                <li><h4>Tech Used</h4><p><?php echo $row[ 'tech' ]; ?><p></li>
            	 	
            </div>

            <div class="col13 fr">

                
                <li><a href="<?php echo $row['siteurl']; ?>" target="_blank">
                VIEW PROJECT
                </a></li>

                <li><a href="<?php echo $row['giturl']; ?>" target="_blank">
                SOURCE CODE
                </a></li>
           

            </div>

            <div class="clear"></div>




        </section>  
        
    </main>    
    
    
</body>

</html>

