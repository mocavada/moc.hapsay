<!-- MAIN PAGE -->	
<?php 

	$result = query_all( $db );
	$counter = 0;

?>
<body>

<?php include( 'includes/templates/header.tpl.php' ); ?>
	
<main class="main-container">
	
	<section id="galleryContainer">	


		<div id="galleryPage" class="portfolioContainer">
		
				<?php while( $row = mysqli_fetch_assoc( $result ) ): ?>

				
					
						<div id="galleryCard" style="background-image: url(<?php echo SITE_ROOT . '/' . LARGE_FOLDER . $row[ 'file_name' ]; ?>);" class="<?php echo $row[ 'type' ]; ?> center fl col13">
							
						
							<li><a href="<?php echo $row['siteurl']; ?>" target="_blank">
							VIEW SITE
							</a></li>

							<li><a href="<?php echo $row['giturl']; ?>" target="_blank">
							INSPECT CODE
							</a></li>

							<h3><a href="?page=preview&amp;id=<?php echo $row[ 'id' ] ;?>">|&nbsp;TECHNOLOGY&nbsp;|</a></h3>

							<li><p><?php echo $row[ 'tech' ]; ?></p></li>

							<h3><a href="?page=preview&amp;id=<?php echo $row[ 'id' ] ;?>"><strong><?php echo $row[ 'title' ]; ?></strong></a></h3>

							<li><p><?php echo $row[ 'description' ]; ?><p></li>

						

						</div>	
						
					
					
					<?php $counter++; 
					while( ($counter % 3 ) == 0){
					echo "<div class=\"clear\" \"spacer\"></div>";
					break;
				
				}
			?>
			
				<?php endwhile; ?>

		</div>

	</section>

</main>	

<?php include( 'includes/templates/footer.tpl.php' ); ?>
</body>


</html>



