<!doctype html>
<html>
<head>
<!--GOOGLE ANALYTICS (gtag.js)-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107615923-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-107615923-1');
	</script>
	
<!--MOBILE METAS-->
    <meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
<!--SEO METAS-->
 	<meta name="description"  content="Marc Cavada Graphic Design and Web Development Portfolio">
    <meta name="keywords"  content="Web Developer, Graphic Design, Logo Design, Wordpress Developer, Front End Developer">
	
<!--SITE TTILE-->
    <title><?php echo SITE_TITLE ?></title>
	
<!-- FAVICON -->
	<link rel="icon" href="/favicon.png">
	
	
<!--FONT AWESOME-->
	<script defer src="js/fontawesome-all.js"></script>
	<script defer src="js/fa-v4-shims.min.js"></script>
	

 <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
     <![endif]-->	
	
<!--jQUERY-->
	<script src="http://code.jquery.com/jquery-1.12.4.js"></script>
	
<!--jQUERY FALLBACK-->
	<script>
	if (!window.jQuery) {
		document.write('<script src="/js/jquery-1.12.4.js"><\/script>');
	}
	</script>
	
<!--SMOOTH SCROLL JQUERY-->
	<script src="js/jquery.smoothscroll.js"></script>	
	
<!--ISOTOPE JQUERY-->
	<script src="js/jquery.isotope.js"></script>
	
<!--ISOTOPE HOOKUP-->
	<script>
	 $(window).load(function(){
		var $container = $('.portfolioContainer');//THIS IS THE NAME OF THE CLASS FOR THE CONTAINER THAT WILL HOLD THE PORTFOLIO IMAGES
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,      //TIMING IN MS
				easing: 'linear',   //EASING
				queue: false
			}
		});

		$('.portfolioFilter a').click(function(){
			$('.portfolioFilter .current1').removeClass('current1');
			$(this).addClass('current1');

			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,     //TIMING IN MS
					easing: 'linear',  //EASING
					queue: false
				}
			 });
			 return false;
		}); 
	}); 
	</script>
	
	
<!--JAVASCRIPT LINK-->

<script src="js/main.js"></script>	

<link rel="stylesheet" href="css/styles.css">
			
	
</head>
		